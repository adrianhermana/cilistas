<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">   
    <h1>Ejercicio 1 consultas</h1> 
    <p>Ejercicios del 1 al 11</p> 
    </div>

    
    <div class="body-content">

        <div class="row">
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 1</h2>
                <p>listar las edades de los ciclistas (sin repetidos)</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta1'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta1a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 2</h2>
                <p>listar las edades de los ciclistas de Artiach</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta2'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta2a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 3</h2>
                <p>listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta3'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta3a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 4</h2>
                <p>listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta4'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta4a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 5</h2>
                <p>listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta5'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta5a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 6</h2>
                <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta6'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta6a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 7</h2>
                <p>lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta7'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta7a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 8</h2>
                <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta8'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta8a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 9</h2>
                <p>Listar el nombre de los puertos cuya altura sea mayor de 1500 </p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta9'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta9a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 10</h2>
                <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta10'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta10a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 11</h2>
                <p>Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta11'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta11a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    </div>
</div>
